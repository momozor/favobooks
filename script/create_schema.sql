PRAGMA foreign_keys = ON;

CREATE TABLE users (
    user_id INTEGER PRIMARY KEY,
    username TEXT NOT NULL,
    password TEXT NOT NULL
);


CREATE TABLE books (
    book_id INTEGER PRIMARY KEY,
    user_id INTEGER REFERENCES users(user_id)
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    title TEXT NOT NULL,
    rating INTEGER NOT NULL,
    author TEXT NOT NULL,
    date_published TEXT NOT NULL
);
