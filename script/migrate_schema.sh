#!/usr/bin/env bash

sqlite3 favobooks.db < script/create_schema.sql

dbicdump -o dump_directory=./lib \
        -o debug=1 \
        Favobooks::Schema \
        'dbi:SQLite:dbname=favobooks.db'

