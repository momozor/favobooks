package Favobooks::Model::Users;

use strict;
use warnings;

use Mojo::Base 'MojoX::Model';

sub register_user
{
    my ($self, $username, $password) = @_;

    die("Username and password cannot be empty!\n")
        unless length($username) && length($password);

    # Abort if user already exists.
    my $user_exists = $self->app->db->resultset('User')->find(
                                                        {
                                                          username => $username
                                                        }
    );

    die("User is already exists! Try to login instead of register.\n")
        if length($user_exists);

    my $user = $self->app->db->resultset('User')->create(
                                                       {
                                                         username => $username,
                                                         password => $password
                                                       }
    );

    return $user;
}

sub login_user
{
    my ($self, $username, $password) = @_;

    die("Username and password cannot be empty!\n")
        unless length($username) && length($password);

    my $user = $self->app->db->resultset('User')->find(
                                                       {
                                                         username => $username,
                                                         password => $password
                                                       }
                                                      );

    die("Account with the username or password doesn't exist.\n")
        unless length($user);

    return $user;
}

1;
