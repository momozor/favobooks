package Favobooks::Model::Books;

use strict;
use warnings;

use Mojo::Base 'MojoX::Model';
use DateTime;
use Text::Autoformat qw(autoformat);

# List all books.
sub list_books
{
    my $self = shift;

    my $books = $self->app->db->resultset('Book');

    if ($books->count == 0)
    {
        return undef;
    }

    else
    {
        return $books;
    }
}

# Create/add a new book.
sub create_book
{
    my ($self, $title, $rating, $author, $user_id) = @_;

    die("Title, rating and author name cannot be empty! Please try again.\n")
        unless length($title) && length($rating) && length($author);

    die("Author name must begin with alphabets. You may include period '.' punctuations. Please try again.\n"
       )
        unless $author =~ /^[a-zA-Z]+\s*\.*/;

    die("Rating must be an integer value! (0 to 10). Please try again.\n")
        unless $rating =~ /^[0-9]|10$/ && ($rating >= 0 && $rating <= 10);

    my $created_book = $self->app->db->resultset('Book')->create(
                          {
                            user_id => $user_id,
                            title   => $title,
                            rating  => $rating,
                            author  => autoformat($author, { case => 'title' }),
                            date_published => DateTime->now->iso8601,
                          }
    );

    return $created_book;
}

# Find specific book
sub find_book
{
    my ($self, $book_id) = @_;

    die("Book ID must be an integer value!\n")
        unless $book_id =~ /^[0-9]+$/;

    return $self->app->db->resultset('Book')->find(
                                                   {
                                                     book_id => $book_id
                                                   }
                                                  );
}

# Check if authenticated user owns this book
sub same_book_owner
{
    my ($self, $book_id, $user_id) = @_;

    my $book = find_book($self, $book_id);

    if ($book->user_id eq $user_id)
    {
        return 1;
    }

    elsif ($book->user_id ne $user_id)
    {
        return 0;
    }
}

sub update_book
{
    my ($self, $book_id, $user_id, $title, $rating, $author) = @_;

    my $book = find_book($self, $book_id);

    if (same_book_owner($self, $book_id, $user_id))
    {
        $book->update(
                      {
                        title  => $title,
                        rating => $rating,
                        author => $author
                      }
                     );
    }
}

# Delete/remove an existing book.
sub delete_book
{
    my ($self, $book_id, $user_id, $title, $rating, $author) = @_;

    my $book = find_book($self, $book_id);

    # To delete book, the authenticated user id must be the same
    # as the created book user id

    if (same_book_owner($self, $book_id, $user_id))
    {
        $book->delete;
    }

    elsif (!same_book_owner($self, $book_id, $user_id))
    {
        die("Book " . $book->book_id . " is not owned by you to be deleted.\n");
    }

    else
    {
        die("Book with ID $book_id doesn't exist. Aborting deletion.\n");
    }
}

1;
