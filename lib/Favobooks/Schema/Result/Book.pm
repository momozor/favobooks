use utf8;

package Favobooks::Schema::Result::Book;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Favobooks::Schema::Result::Book

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<books>

=cut

__PACKAGE__->table("books");

=head1 ACCESSORS

=head2 book_id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 user_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 1

=head2 title

  data_type: 'text'
  is_nullable: 0

=head2 rating

  data_type: 'integer'
  is_nullable: 0

=head2 author

  data_type: 'text'
  is_nullable: 0

=head2 date_published

  data_type: 'text'
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
                         "book_id",
                         {
                            data_type         => "integer",
                            is_auto_increment => 1,
                            is_nullable       => 0
                         },
                         "user_id",
                         {
                            data_type      => "integer",
                            is_foreign_key => 1,
                            is_nullable    => 1
                         },
                         "title",
                         { data_type => "text", is_nullable => 0 },
                         "rating",
                         { data_type => "integer", is_nullable => 0 },
                         "author",
                         { data_type => "text", is_nullable => 0 },
                         "date_published",
                         { data_type => "text", is_nullable => 0 },
                        );

=head1 PRIMARY KEY

=over 4

=item * L</book_id>

=back

=cut

__PACKAGE__->set_primary_key("book_id");

=head1 RELATIONS

=head2 user

Type: belongs_to

Related object: L<Favobooks::Schema::Result::User>

=cut

__PACKAGE__->belongs_to(
                        "user",
                        "Favobooks::Schema::Result::User",
                        { user_id => "user_id" },
                        {
                           is_deferrable => 0,
                           join_type     => "LEFT",
                           on_delete     => "CASCADE",
                           on_update     => "CASCADE",
                        },
                       );

# Created by DBIx::Class::Schema::Loader v0.07047 @ 2018-02-04 20:19:22
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:eqeDQdE/sYO+szOVJndPqw

# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
