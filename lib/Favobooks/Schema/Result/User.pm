use utf8;

package Favobooks::Schema::Result::User;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Favobooks::Schema::Result::User

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<users>

=cut

__PACKAGE__->table("users");

=head1 ACCESSORS

=head2 user_id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 username

  data_type: 'text'
  is_nullable: 0

=head2 password

  data_type: 'text'
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
                         "user_id",
                         {
                            data_type         => "integer",
                            is_auto_increment => 1,
                            is_nullable       => 0
                         },
                         "username",
                         { data_type => "text", is_nullable => 0 },
                         "password",
                         { data_type => "text", is_nullable => 0 },
                        );

=head1 PRIMARY KEY

=over 4

=item * L</user_id>

=back

=cut

__PACKAGE__->set_primary_key("user_id");

=head1 RELATIONS

=head2 books

Type: has_many

Related object: L<Favobooks::Schema::Result::Book>

=cut

__PACKAGE__->has_many(
                      "books",
                      "Favobooks::Schema::Result::Book",
                      { "foreign.user_id" => "self.user_id" },
                      { cascade_copy      => 0, cascade_delete => 0 },
                     );

# Created by DBIx::Class::Schema::Loader v0.07047 @ 2018-02-04 20:00:27
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:YPfORzSjztmQCHj//wYimA

# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
