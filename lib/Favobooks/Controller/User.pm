package Favobooks::Controller::User;
use Mojo::Base 'Mojolicious::Controller';

sub form_register_page
{
    my $self = shift;

    $self->render(template => '/user/form_register_page');
}

sub form_register_page_do
{
    my $self = shift;

    my $username = $self->param('username');
    my $password = $self->param('password');

    my $registered_user =
        $self->model('Users')->register_user($username, $password);

    $self->session(logged_in => 1);
    $self->session(username  => $registered_user->username);
    $self->session(uid       => $registered_user->id);

    $self->redirect_to('book_list');
}

sub form_login_page
{
    my $self = shift;

    $self->render(template => '/user/form_login_page');
}

sub form_login_page_do
{
    my $self = shift;

    my $username = $self->param('username');
    my $password = $self->param('password');

    my $authenticated_user =
        $self->model('Users')->login_user($username, $password);

    $self->session(logged_in => 1);
    $self->session(username  => $authenticated_user->username);
    $self->session(uid       => $authenticated_user->id);

    $self->flash(message => 'You are logged in. Welcome.');
    $self->redirect_to('book_list');
}

sub logout_do
{
    my $self = shift;

    $self->session(expires => 1);
    $self->redirect_to('index');
}

sub is_authen
{
    my $self = shift;

    return 0,
        $self->flash(message =>
         'You are not logged in to create a new book. Please log in to proceed.'
        ), $self->redirect_to('form_login_page')
        if not length($self->session('logged_in'));

    return 1 if $self->session('logged_in') == 1;
}

1;

