package Favobooks::Controller::Book;
use Mojo::Base 'Mojolicious::Controller';

sub list
{
    my $self = shift;

    $self->stash(books => $self->model('Books')->list_books);
}

sub form_create
{
    my $self = shift;
}

sub form_create_do
{
    my $self = shift;

    my $title   = $self->param('title');
    my $rating  = $self->param('rating');
    my $author  = $self->param('author');
    my $user_id = $self->session('uid');

    $self->stash(book => $self->model('Books')
                 ->create_book($title, $rating, $author, $user_id));

    $self->render(template => 'book/create_done');
}

sub form_update
{
    my $self = shift;

    my $book_id = $self->param('id');
    my $user_id = $self->session('uid');

    $self->model('Books')->same_book_owner($book_id, $user_id);

    my $book = $self->model('Books')->find_book($book_id);

    $self->stash(book_title  => $book->title);
    $self->stash(book_rating => $book->rating);
    $self->stash(book_author => $book->author);

    $self->session('book_id'     => $book_id);
    $self->session('book_title'  => $book->title);
    $self->session('book_rating' => $book->rating);
    $self->session('book_author' => $book->author);

    $self->render(template => 'book/form_update');
}

sub form_update_do
{
    my $self = shift;

    my $book_id     = $self->session('book_id');
    my $user_id     = $self->session('uid');
    my $book_title  = $self->session('book_title');
    my $book_rating = $self->session('book_rating');
    my $book_author = $self->session('book_author');

    $self->model('Books')->same_book_owner($book_id, $user_id);

    $self->model('Books')->update_book($book_id,     $user_id, $book_title,
                                       $book_rating, $book_author);

    # remove the temporary sessions
    delete $self->session->{book_id};
    delete $self->session->{book_title};
    delete $self->session->{book_rating};
    delete $self->session->{book_author};

    $self->redirect_to('book_list');
}

sub delete
{
    my $self = shift;

    my $book_id = $self->param('id');
    my $user_id = $self->session('uid');

    $self->model('Books')->delete_book($book_id, $user_id);
    $self->redirect_to('book_list');
}

1;
