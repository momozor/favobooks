package Favobooks;
use Mojo::Base 'Mojolicious';
use Favobooks::Schema;
use strict;
use warnings;

# This method will run once at server start
sub startup
{
    my $self = shift;

    my $config = $self->plugin('Config', file => 'favobooks.conf');
    $self->mode($config->{mode});

    my $database_driver = $config->{database}{driver};
    my $database_name = $config->{database}{name};
    my $schema = Favobooks::Schema->connect("dbi:$database_driver:$database_name");
    $self->helper(db => sub { return $schema });

    $self->plugin('Model');
    $self->plugin('CSRFProtect');

    $self->sessions->default_expiration(3600);

    # Router instance
    my $r = $self->routes;

    # Root paths
    $r->get('/')->name('index')->to('root#index');

    # Book paths
    $r->get('/book/list')->name('book_list')->to('book#list');

    $r->any('/book/delete/:id')->name('delete_book')->to('book#delete');

    # Authentication/User paths.
    $r->get('/user/form_register_page')->name('form_register_page')
        ->to('user#form_register_page');
    $r->post('/user/form_register_page_do')->name('register_page_do')
        ->to('user#form_register_page_do');

    $r->get('/user/form_login_page')->name('form_login_page')
        ->to('user#form_login_page');
    $r->post('/user/form_login_page_do')->name('login_page_do')
        ->to('user#form_login_page_do');

    # Temporary confirmation page.
    $r->get('/user/login_done')->name('login_done');

    $r->get('/user/logout_do')->name('logout_do')->to('user#logout_do');

    my $user_only = $r->under('/book')->to('user#is_authen');

    $user_only->get('/form_create')->name('form_add_book')
        ->to('book#form_create');
    $user_only->post('/form_create_do')->name('add_book_do')
        ->to('book#form_create_do');
    $user_only->get('/create_done')->name('create_done');
    $user_only->get('/form_update/:id')->name('form_update_book')
        ->to('book#form_update');
    $user_only->post('/form_update_do')->name('update_book_do')
        ->to('book#form_update_do');
    $user_only->any('/delete/:id')->name('delete_book')->to('book#delete');
}

1;
