[![Kritika Analysis Status](https://kritika.io/users/faraco/repos/7404362859411256/heads/master/status.svg)](https://kritika.io/users/faraco/repos/7404362859411256/heads/master/)
# favobooks

favobooks is a book library web application.

## Getting Started

### Prerequisites

* perl >= 5.10
* App::cpanminus
* sqlite3
* Carton

## Starting local web development server (morbo)

> Make sure you ran `carton install  && carton exec bash script/migrate_schema.sh` before running the command below!

Run the following:

`carton exec morbo script/favobooks`

and then navigate with your web browser to http://localhost:3000 to see the application.

## Running the tests

Run the following:

`carton exec script/favobooks test`

## Author

* [Momozor](https://github.com/Momozor) <skelic3@gmail.com>
        
## License

This project is licensed under the BSD-2-Clause License - see the LICENSE file for details.
    
