requires 'Mojolicious', '==', '7.61';
requires 'DBIx::Class', '==', '0.082840';
requires 'DateTime', '==', '1.45';
requires 'Mojolicious::Plugin::Model', '==', '0.11';
requires 'Mojolicious::Plugin::CSRFProtect', '==', '0.04';
requires 'Text::Autoformat', '==', '1.74';
requires 'SQL::Translator';
requires 'DBIx::Class::Schema::Loader';
requires 'Mojolicious::Plugin::CSRFProtect';
