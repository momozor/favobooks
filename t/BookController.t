use Mojo::Base -strict;

use Test::More;
use Test::Mojo;

my $t = Test::Mojo->new('Favobooks');

$t->get_ok('/book/list')
    ->status_is(200);

$t->get_ok('/book/form_create')
    ->status_is(302);

done_testing;
