use Mojo::Base -strict;

use Test::More;
use Test::Mojo;

my $t = Test::Mojo->new('Favobooks');

$t->get_ok('/user/form_register_page')
    ->status_is(200);

$t->get_ok('/user/form_login_page')
    ->status_is(200);

done_testing;
