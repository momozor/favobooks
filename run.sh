#!/bin/bash

MODE="$1"

[ -z "$MODE" ] && carton exec morbo script/favobooks

[ "$MODE" == 'test' ] && carton exec script/favobooks test
